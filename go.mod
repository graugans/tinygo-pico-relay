module gitlab.com/graugans/tinygo-pico-relay

go 1.21

require (
	github.com/gorilla/mux v1.8.0
	github.com/spf13/cobra v1.7.0
	github.com/stretchr/testify v1.8.1
	go.bug.st/serial v1.5.0
)

require (
	github.com/creack/goselect v0.1.2 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20220829200755-d48e67d00261 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
