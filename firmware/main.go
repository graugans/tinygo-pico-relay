package main

import (
	"bufio"
	"fmt"
	"machine"
	"os"

	"gitlab.com/graugans/tinygo-pico-relay/channel"
	"gitlab.com/graugans/tinygo-pico-relay/protocol"
)

const version = "v1.3.0"

func readFromSerial(ch chan<- protocol.Command) {
	reader := bufio.NewReader(os.Stdin)
	command := protocol.Command{}
	for {
		err := command.Parse(reader)
		if err != nil {
			fmt.Print(protocol.MarshalError(err))
			continue
		}
		ch <- command
	}
}

func getChannelByIndex(index uint, channels []*channel.Channel) (*channel.Channel, error) {
	const minChannel = 1
	var maxChannel uint = uint(len(channels))
	if index > maxChannel || index < minChannel {
		return nil, fmt.Errorf("Invalid index provided: %d. Channel must be in range [%d,%d]",
			index,
			minChannel,
			maxChannel,
		)
	}
	return channels[index-1], nil
}

func main() {
	led.Configure(machine.PinConfig{Mode: machine.PinOutput})
	led.Set(true)
	commands := make(chan protocol.Command)
	go readFromSerial(commands) // make it parallel
	for {
		command := <-commands
		switch command.Type {
		case protocol.Get:
			ch, err := getChannelByIndex(command.Channel, Channels)
			if err != nil {
				fmt.Print(protocol.MarshalError(err))
			}
			res := protocol.Response{
				Success: true,
				Channel: ch.Index(),
				Status:  ch.Get(),
			}
			fmt.Print(string(res.Marshal()))
		case protocol.Set:
			ch, err := getChannelByIndex(command.Channel, Channels)
			if err != nil {
				fmt.Print(protocol.MarshalError(err))
				continue
			}
			ch.Set(command.Status)
			res := protocol.Response{
				Success: true,
				Channel: ch.Index(),
				Status:  ch.Get(),
			}
			fmt.Print(string(res.Marshal()))
		case protocol.Version:
			res := protocol.Response{
				Success: true,
				Message: fmt.Sprintf("%s", version),
			}
			fmt.Print(string(res.Marshal()))
		case protocol.Bootloader:
			enterBootloader()
		case protocol.MaxChannels:
			res := protocol.Response{
				Success:    true,
				MaxChannel: len(Channels),
			}
			fmt.Print(string(res.Marshal()))
		}
	}
}
